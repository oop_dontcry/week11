import { IsNotEmpty, Length } from 'class-validator';

export class CreateProductDto {
  @IsNotEmpty()
  @Length(3, 16)
  name: string;

  @IsNotEmpty()
  price: number;
}
